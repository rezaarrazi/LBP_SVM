# import the necessary packages
from localbinarypattern import LocalBinaryPatterns
from imutils import paths
from joblib import load
import argparse
import cv2
import os

desc = LocalBinaryPatterns(8, 1)
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-m", "--model", required=True,
	help="path to the training model")
ap.add_argument("-e", "--testing", required=True, 
	help="path to the tesitng images")
ap.add_argument("-iw", "--width", required=True,
	help="images width")
ap.add_argument("-ih", "--height", required=True,
	help="images height")
ap.add_argument("-v", "--visualization",
	help="display the image and the prediction")
args = vars(ap.parse_args())

model = load(args["model"])
image_width = int(args["width"])
image_height = int(args["height"])

visualization = False
if args["visualization"] == "true" or args["visualization"] == "True":
    visualization = True

# loop over the testing images
for imagePath in paths.list_images(args["testing"]):
	# load the image, convert it to grayscale, describe it,
	# and classify it
    image = cv2.imread(imagePath)
    resized_image = cv2.resize(image, (image_width, image_height))
    gray = cv2.cvtColor(resized_image, cv2.COLOR_BGR2GRAY)
    hist = desc.describe(gray)
    prediction = model.predict(hist.reshape(1, -1))
    print(prediction[0])

    if visualization == True:
        # display the image and the prediction
        output_image = cv2.resize(image, (300, 300))
        cv2.putText(output_image, prediction[0], (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
            1.0, (0, 0, 255), 3)
        cv2.imshow("Image", output_image)
        cv2.waitKey(0)