# import the necessary packages
from skimage import feature
import numpy as np
from matplotlib import pyplot as plt
import cv2

class LocalBinaryPatterns:
    def __init__(self, numPoints, radius):
        # store the number of points and radius
        self.numPoints = numPoints
        self.radius = radius

    def __get_pixel__(self, img, center, x, y):
        new_value = 0
        try:
            if img[x][y] >= center:
                new_value = 1
        except:
            pass
        return new_value

    def __lbp_calculated_pixel__(self, img, x, y):
        center = img[x][y]
        val_ar = []
        val_ar.append(self.__get_pixel__(img, center, x-1, y+1))     # top_right
        val_ar.append(self.__get_pixel__(img, center, x, y+1))       # right
        val_ar.append(self.__get_pixel__(img, center, x+1, y+1))     # bottom_right
        val_ar.append(self.__get_pixel__(img, center, x+1, y))       # bottom
        val_ar.append(self.__get_pixel__(img, center, x+1, y-1))     # bottom_left
        val_ar.append(self.__get_pixel__(img, center, x, y-1))       # left
        val_ar.append(self.__get_pixel__(img, center, x-1, y-1))     # top_left
        val_ar.append(self.__get_pixel__(img, center, x-1, y))       # top

        binary = int("".join(map(str, val_ar)), 2)
        return binary
    
    def __lbp__(self, image):
        rows, cols = image.shape[:2]
        new_image = np.zeros((rows, cols, 3), np.uint8)
        for i in range(0, rows):
            for j in range(0, cols):
                new_image[i,j] = self.__lbp_calculated_pixel__(image, i, j)
        return new_image

    def __mb_lbp__(self, image, block_size):
        rows, cols = image.shape[:2]
        new_image = np.zeros((rows, cols, 3), np.uint8)
        total = block_size*block_size
        for i in range(0, rows):
            for j in range(0, cols):
                data = []
                for y in range(i,i+(block_size*3),block_size):
                    for x in range(j,j+(block_size*3),block_size):
                        sum = 0
                        for _ in image[y:y+block_size, x:x+block_size]:
                            for value in _:
                                sum += value
                        data.append(int(sum / total))
                
                binary_data = []
                center_index = 4
                for q in range(len(data)):
                    if q != center_index :
                        if data[q]>=data[center_index]:
                            binary_data.append(1)
                        else:
                            binary_data.append(0)
                binary = int("".join(map(str, binary_data)), 2)
                new_image[i,j] = binary
        return new_image

    def __show_output__(self, output_list):
        output_list_len = len(output_list)
        figure = plt.figure()
        for i in range(output_list_len):
            current_dict = output_list[i]
            current_img = current_dict["img"]
            current_xlabel = current_dict["xlabel"]
            current_ylabel = current_dict["ylabel"]
            current_xtick = current_dict["xtick"]
            current_ytick = current_dict["ytick"]
            current_title = current_dict["title"]
            current_type = current_dict["type"]
            current_plot = figure.add_subplot(1, output_list_len, i+1)
            if current_type == "gray":
                current_plot.imshow(current_img, cmap = plt.get_cmap('gray'))
                current_plot.set_title(current_title)
                current_plot.set_xticks(current_xtick)
                current_plot.set_yticks(current_ytick)
                current_plot.set_xlabel(current_xlabel)
                current_plot.set_ylabel(current_ylabel)
            elif current_type == "histogram":
                current_plot.plot(current_img, color = "black")
                current_plot.set_xlim([0,260])
                current_plot.set_title(current_title)
                current_plot.set_xlabel(current_xlabel)
                current_plot.set_ylabel(current_ylabel)            
                ytick_list = [int(i) for i in current_plot.get_yticks()]
                current_plot.set_yticklabels(ytick_list,rotation = 90)

        plt.show()
    
    def describe(self, image, eps=1e-7, visualization=False):
        # lbp = feature.local_binary_pattern(image, self.numPoints, self.radius, method="uniform")
        lbp = self.__lbp__(image)

        (hist, _) = np.histogram(lbp.ravel(), bins=256)
        
		# normalize the histogram
        hist = hist.astype("float")
        hist /= (hist.sum() + eps)

        if visualization == True :
            output_list = []
            output_list.append({
                "img": image,
                "xlabel": "",
                "ylabel": "",
                "xtick": [],
                "ytick": [],
                "title": "Gray Image",
                "type": "gray"        
            })
            output_list.append({
                "img": lbp,
                "xlabel": "",
                "ylabel": "",
                "xtick": [],
                "ytick": [],
                "title": "LBP Image",
                "type": "gray"
            })    
            output_list.append({
                "img": hist,
                "xlabel": "Bins",
                "ylabel": "Number of pixels",
                "xtick": None,
                "ytick": None,
                "title": "Histogram(LBP)",
                "type": "histogram"
            })

            self.__show_output__(output_list)
                                    
            cv2.waitKey(0)
            cv2.destroyAllWindows()

		# return the histogram of Local Binary Patterns
        return hist