python train.py --positive ..\..\Ichiro\datasetbola\positif\pos_top --negative ..\..\Ichiro\datasetbola\negatif\neg_top --width 32 --height 32 --numsamples 58080 --model training\classify.sav

python recognize.py --model training\classify.sav --testing ..\..\Ichiro\datasetbola\positif\pos_top --width 32 --height 32 --visualization true