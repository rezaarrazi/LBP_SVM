# import the necessary packages
from localbinarypattern import LocalBinaryPatterns
from sklearn.svm import LinearSVC
from imutils import paths
from joblib import dump
from random import random
import argparse
import cv2
import os

# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-p", "--positive", required=True,
	help="path to the positive training images")
ap.add_argument("-n", "--negative", required=True,
	help="path to the negative training images")
ap.add_argument("-iw", "--width", required=True,
	help="images width")
ap.add_argument("-ih", "--height", required=True,
	help="images height")
ap.add_argument("-ns", "--numsamples", required=True,
	help="amount of negative samples")
ap.add_argument("-m", "--model", required=True,
	help="path to the training model result")

args = vars(ap.parse_args())

# initialize the local binary patterns descriptor along with
# the data and label lists
desc = LocalBinaryPatterns(24, 8)
positive_image_list = []
negative_image_list = []
negative_sample_image_list = []
data = []
labels = []

image_width = int(args["width"])
image_height = int(args["height"])
num_negative_samples = int(args["numsamples"])

# loop over the positive training images
print("Load positive images")
for imagePath in paths.list_images(args["positive"]):
	image = cv2.imread(imagePath)
	resized_image = cv2.resize(image, (image_width, image_height))
	positive_image_list.append(resized_image)
print("total %d positive images[done]"% (len(positive_image_list)))

# loop over the negative training images
print("Load negative images")
for imagePath in paths.list_images(args["negative"]):
	image = cv2.imread(imagePath)
	resized_image = cv2.resize(image, (320, 240))
	negative_image_list.append(resized_image)
print("total %d negative images[done]"% (len(negative_image_list)))

# patch all negative training images
print("generate samples from negative images")
num_negative_samples_perImage = int(num_negative_samples / len(negative_image_list))
for negativeImage in negative_image_list:
	rows, cols = negativeImage.shape[:2]
	for _ in range(num_negative_samples_perImage):
		x = int(random() * ( cols - image_width ))
		y = int(random() * ( rows - image_height ))
		crop_img = negativeImage[y:y+image_height, x:x+image_width]
		# cv2.imshow("cropped", crop_img)
		# cv2.waitKey(1)
		negative_sample_image_list.append(crop_img)
print("total %d negative samples[done]"% (len(negative_sample_image_list)))

print("compute LBP for positive data")
for positive_image in positive_image_list:
	# load the image, convert it to grayscale, and describe it
	gray = cv2.cvtColor(positive_image, cv2.COLOR_BGR2GRAY)
	hist = desc.describe(gray, visualization=False)

	labels.append("positive")
	data.append(hist)

print("compute LBP for negative data")
for negative_image in negative_sample_image_list:
	# load the image, convert it to grayscale, and describe it
	gray = cv2.cvtColor(negative_image, cv2.COLOR_BGR2GRAY)
	hist = desc.describe(gray)

	labels.append("negative")
	data.append(hist)

print("Training data...")
# train a Linear SVM on the data
model = LinearSVC(C=100.0, random_state=42)
model.fit(data, labels)

dump(model, args["model"])

# evaluation